package frc.robot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.core.sym.Name;
import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.auto.NamedCommands;
import com.pathplanner.lib.commands.FollowPathHolonomic;
import com.pathplanner.lib.commands.FollowPathWithEvents;
import com.pathplanner.lib.commands.PathPlannerAuto;
import com.pathplanner.lib.path.PathConstraints;
import com.pathplanner.lib.path.PathPlannerPath;
import com.pathplanner.lib.path.PathPlannerTrajectory;
import com.pathplanner.lib.util.HolonomicPathFollowerConfig;
import com.pathplanner.lib.util.PIDConstants;
import com.pathplanner.lib.util.ReplanningConfig;
import com.revrobotics.ColorSensorV3;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.shuffleboard.EventImportance;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.PrintCommand;
import edu.wpi.first.wpilibj2.command.RepeatCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.Systems.Frizzle;
import frc.robot.Systems.StockerOdometry;
import frc.robot.commands.SwerveDefaultCommand;
import frc.robot.commands.TurnTo;
import frc.robot.commands.AutoMoves.AutoClimb;
import frc.robot.commands.Collector.CollectorToShoot;
import frc.robot.commands.Collector.CollectorToShootAuto;
import frc.robot.commands.Collector.CollectorToTrap;
import frc.robot.commands.Collector.CompleteCollectPosition;
import frc.robot.commands.Collector.MoreCollectCommand;
import frc.robot.commands.Collector.ResetCollectorCommand;
import frc.robot.commands.Collector.SetShooterToPosition;
import frc.robot.commands.Collector.Shoot;
import frc.robot.commands.Collector.ShootNoneDistance;
import frc.robot.commands.Elevator.ElevatorCommand;
import frc.robot.commands.Elevator.SetClimbPosition;
import frc.robot.commands.Elevator.SetElevatorPosition;
import frc.robot.commands.Elevator.SetSlidePosition;
import frc.robot.commands.ShooterDefaultCommand;
import frc.robot.subsystems.ElevatorSubsystem;
import frc.robot.subsystems.shooterSubsystem;
import frc.robot.subsystems.Swerve.SwerveSubsystem;
import frc.util.PS5ControllerDriverConfig;
import frc.util.Vision;

public class RobotContainer {

    // Shuffleboard auto chooser
    // private final SendableChooser<Command> autoCommand = new SendableChooser<>();

    //shuffleboard tabs
    // The main tab is not currently used. Delete the SuppressWarning if it is used.
    @SuppressWarnings("unused")
    private final ShuffleboardTab mainTab = Shuffleboard.getTab("Main");
    private final ShuffleboardTab drivetrainTab = Shuffleboard.getTab("Drive");
    private final ShuffleboardTab swerveModulesTab = Shuffleboard.getTab("Swerve Modules");
    private final ShuffleboardTab autoTab = Shuffleboard.getTab("Auto");
    private final ShuffleboardTab controllerTab = Shuffleboard.getTab("Controller");
    private final ShuffleboardTab visionTab = Shuffleboard.getTab("Vision");

    private final Vision cameraVisiom  = new Vision("MavriXCam" , Constants.VisionConstants.LimeLightTeleop);
    private final Vision limelightVision  = new Vision("MavriXCam" , Constants.VisionConstants.LimeLightAuto);;

 
    // The robot's subsystems are defined here...
    private SwerveSubsystem drive = new SwerveSubsystem(drivetrainTab, swerveModulesTab, cameraVisiom , limelightVision);

    // Controllers are defined here
    PS5ControllerDriverConfig driver;

    Joystick driverJoystick;
    shooterSubsystem m_shooterSubsystem;
    Joystick systemJoystick;
    ElevatorSubsystem elevator;
    ColorSensorV3 colorSensor;

    Joystick playJoystick;

   Pose2d[] pose2dTargets = {new Pose2d(0,0,new Rotation2d(12)), new Pose2d(3,1,new Rotation2d())};
   public RobotContainer() {
      m_shooterSubsystem = new shooterSubsystem();
      elevator = new ElevatorSubsystem();
      driver = new PS5ControllerDriverConfig(this.drive, controllerTab , Constants.Telemetry);
      driver.configureControls();

      driverJoystick = new Joystick(0);
      systemJoystick = new Joystick(1);
      playJoystick = new Joystick(2);

      drive.setDefaultCommand(new SwerveDefaultCommand(this.drive, driver , controllerTab));
      elevator.setDefaultCommand(new ElevatorCommand(elevator, playJoystick));
      m_shooterSubsystem.setDefaultCommand(new ShooterDefaultCommand(m_shooterSubsystem, drive,  systemJoystick));
        
      // This is really annoying so it's disabled
      DriverStation.silenceJoystickConnectionWarning(true);
      LiveWindow.disableAllTelemetry(); // LiveWindow is causing periodic loop overruns
      LiveWindow.setEnabled(false);

      // autoTab.add("Auto Chooser", autoCommand);

        
      // if(DriverStation.getAlliance().get() == Alliance.Red){
            drive.setYaw(new Rotation2d(0));
        // }else{
        //     drive.setYaw(new Rotation2d(180));

        //  }


        if (Constants.Telemetry) loadCommandSchedulerShuffleboard();

        driver.setupShuffleboard();

    configureBindings();
  }
     
  private void configureBindings() {

    new JoystickButton(driverJoystick, 6).onTrue(new StockerOdometry(drive , driverJoystick));
    new JoystickButton(driverJoystick, 3).onTrue(new Frizzle(drive, driverJoystick, new Pose2d(2.9, 7, new Rotation2d(90))));


    new JoystickButton(systemJoystick, 8).whileTrue(new CollectorToShoot(m_shooterSubsystem))
          .onFalse(new SequentialCommandGroup(new ResetCollectorCommand(m_shooterSubsystem), new MoreCollectCommand(m_shooterSubsystem)));
    new JoystickButton(systemJoystick, 7).whileTrue(new CollectorToTrap(m_shooterSubsystem))
          .onFalse(new ResetCollectorCommand(m_shooterSubsystem));

    new JoystickButton(playJoystick, 1).onTrue(new AutoClimb(elevator,m_shooterSubsystem));

    new JoystickButton(driverJoystick, 2).onTrue(new StockerOdometry(drive, driverJoystick));


    driver.configureControls();

}


  public Command getAutonomousCommand() {

    NamedCommands.registerCommand("collect",  new CollectorToShoot(m_shooterSubsystem));
    NamedCommands.registerCommand("MoreCollect", new MoreCollectCommand(m_shooterSubsystem));
    NamedCommands.registerCommand("stocker", new StockerOdometry(drive, driverJoystick));
    // NamedCommands.registerCommand("firstShooter", new ShootNoneDistance(m_shooterSubsystem, 16000, 1000));
    NamedCommands.registerCommand("plot", new ShootNoneDistance(m_shooterSubsystem, 18500, 18500));
    NamedCommands.registerCommand("moveArmFirst", new SetShooterToPosition(m_shooterSubsystem, 30000));
    return new SequentialCommandGroup(
        new InstantCommand(() -> this.drive.resetYaw()),
        new InstantCommand(() -> this.drive.resetOdometry(PathPlannerAuto.getStaringPoseFromAutoFile("4PieceRed"))), 
        new InstantCommand(() -> this.drive.resetModulesToAbsolute()),
        drive.getAutoBuilder().buildAuto("4PieceRed"),
        new InstantCommand(() -> this.drive.stop()), 
        new InstantCommand(() -> this.drive.resetModulesToAbsolute())
    );
  }

  public void loadCommandSchedulerShuffleboard() {
      // Set the scheduler to log Shuffleboard events for command initialize, interrupt, finish
      CommandScheduler.getInstance().onCommandInitialize(command -> Shuffleboard.addEventMarker("Command initialized", command.getName(), EventImportance.kNormal));
      CommandScheduler.getInstance().onCommandInterrupt(command -> Shuffleboard.addEventMarker("Command interrupted", command.getName(), EventImportance.kNormal));
      CommandScheduler.getInstance().onCommandFinish(command -> Shuffleboard.addEventMarker("Command finished", command.getName(), EventImportance.kNormal));
  
  }

    public void onTeleopSwerve(){
        drive.setAllInvertedTrue();
    }
    public void onAutoSwerve(){
        drive.setAllInvertedFalse();
    }
}
