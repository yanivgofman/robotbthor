// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Elevator;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.ElevatorSubsystem;

public class SetSlidePosition extends Command {
  ElevatorSubsystem elevatorSubsystem;
  double position;


  public SetSlidePosition(ElevatorSubsystem elevatorSubsystem,double position) {
    this.elevatorSubsystem = elevatorSubsystem;
    this.position = position;
    addRequirements(elevatorSubsystem);

  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    elevatorSubsystem.setSlidePosition(position);
    System.out.println("mvoing slidee");
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return elevatorSubsystem.isSlideAtPosition(3000,position);
  }
}
