// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Elevator;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.ElevatorSubsystem;

public class SetClimbPosition extends Command {
  ElevatorSubsystem climb;
  double position;
  public SetClimbPosition(ElevatorSubsystem climb, double position) {
    this.climb = climb;
    this.position = position;
    addRequirements(climb);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    climb.setClimbPosition(position);
    System.out.println("moving climb");
  }

  @Override
  public void end(boolean interrupted) {
    System.out.println("finished climb position");
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return climb.isClimbAtPosition(2000, position);
  }
}
