// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Collector;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.shooterSubsystem;


public class CompleteCollectPosition extends SequentialCommandGroup {
  shooterSubsystem shooter;
  public CompleteCollectPosition(shooterSubsystem shooter, double position) {
    this.shooter = shooter;
    addCommands(new CollectorToShootAuto(shooter), new SetShooterToPosition(shooter, position), new MoreCollectCommand(shooter));
  }
}
