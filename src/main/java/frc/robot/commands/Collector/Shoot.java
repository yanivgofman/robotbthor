package frc.robot.commands.Collector;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.subsystems.shooterSubsystem;
import frc.robot.subsystems.Swerve.SwerveSubsystem;

public class Shoot extends Command {
  shooterSubsystem shooterSubsystem;
  Timer time;
  boolean hasDelivered = false;
  Timer deliverTimer;
  public Shoot(shooterSubsystem shooterSubsystem) {
      this.shooterSubsystem = shooterSubsystem;
      this.time = new Timer();
      this.deliverTimer = new Timer();

      addRequirements(shooterSubsystem);
  }

  @Override
  public void initialize() {
    shooterSubsystem.shooter(0,0);
    shooterSubsystem.delivery(0);
    time.stop();
    time.reset();
    deliverTimer.stop();
    deliverTimer.reset();
  }

  @Override
  public void execute() {
    shooterSubsystem.shooterVelocity(18500 , 18500);
    shooterSubsystem.setArmPosition(shooterSubsystem.getShootingFunction(Constants.Swerve.distance));
    // System.out.println(" arm " + shooterSubsystem.isArmAtPosition()  + " shooter" + shooterSubsystem.reachedVelocity());
    if(shooterSubsystem.isArmAtPosition() && shooterSubsystem.reachedVelocity()) { 
      time.start();
      if(time.hasElapsed(0.3)) {
        shooterSubsystem.delivery(-0.3);
        hasDelivered = true;
        deliverTimer.start();
      }
      // System.out.println("dafadsf" + timer.get());
    }else {
      shooterSubsystem.delivery(0);
      time.reset();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    time.stop();
    time.reset();
    shooterSubsystem.shooter(0,0);
    shooterSubsystem.delivery(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return deliverTimer.hasElapsed(0.2);
  }
}
