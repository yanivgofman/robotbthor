
package frc.robot.commands.AutoMoves;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.Collector.SetShooterToPosition;
import frc.robot.commands.Elevator.SetClimbPosition;
import frc.robot.commands.Elevator.SetElevatorPosition;
import frc.robot.subsystems.ElevatorSubsystem;
import frc.robot.subsystems.shooterSubsystem;

public class AutoClimb extends SequentialCommandGroup {
  ElevatorSubsystem elevator;
  shooterSubsystem shooter;
  public AutoClimb(ElevatorSubsystem elevator, shooterSubsystem shooter) {
    this.shooter = shooter;
    this.elevator = elevator;
    addCommands(new SetElevatorPosition(elevator, 0), new SetClimbPosition(elevator, -30000),new SetShooterToPosition(shooter, 150000), new SetElevatorPosition(elevator, 900000));
  }
}
