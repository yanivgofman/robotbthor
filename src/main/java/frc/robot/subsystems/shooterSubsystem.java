// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import java.io.Console;
import java.lang.invoke.ConstantCallSite;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.revrobotics.ColorSensorV3;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.I2C.Port;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.util.Falcon500;

public class shooterSubsystem extends SubsystemBase {
  
  /** Creates a new Shotter. */
  TalonFX intakeUpper;
  TalonFX intakeLower;
  TalonFX delivery;
  TalonFX arm;
  ColorSensorV3 noteLimit;
  I2C.Port i2cport = I2C.Port.kOnboard;
  ColorSensorV3 colorSensor;
  public shooterSubsystem() {

    colorSensor = new ColorSensorV3(i2cport);
    intakeLower = Falcon500.createTalon(Constants.CollectorConstants.LowerShootingWheelsId, Constants.CollectorConstants.ShootingWheelsNeutralMode);
    intakeUpper = Falcon500.createTalon(Constants.CollectorConstants.UpperShootingWheelsId, Constants.CollectorConstants.ShootingWheelsNeutralMode);
    arm = Falcon500.createTalon(Constants.CollectorConstants.ArmMotorID, Constants.CollectorConstants.ArmNeutralMode);
    delivery = Falcon500.createTalon(Constants.CollectorConstants.DeliveryMotorId, Constants.CollectorConstants. DelivertNeutralMode);
    
    //ArmConfigs
    arm.config_kP(0, Constants.CollectorConstants.ArmKp);
    arm.config_kI(0, Constants.CollectorConstants.ArmKi);
    arm.config_kD(0, Constants.CollectorConstants.ArmKd);
    arm.config_kF(0,Constants.CollectorConstants.ArmKf);
    arm.configAllowableClosedloopError(0, Constants.CollectorConstants.ArmAllowableError);
    arm.configClosedLoopPeakOutput(0, 1);

    //LowerIntakeConfigs  
    intakeLower.config_kP(0, Constants.CollectorConstants.shootingKp);
    intakeLower.config_kI(0, Constants.CollectorConstants.shootingKi);
    intakeLower.config_kD(0, Constants.CollectorConstants.shootingKd);
    intakeLower.config_kF(0,Constants.CollectorConstants.shootingKf);
    intakeLower.configMaxIntegralAccumulator(0, Constants.CollectorConstants.shootingMaxIntegralAccum);
    intakeLower.configClosedLoopPeriod(0,Constants.CollectorConstants.shootingLoopPeriodMs);
    intakeLower.config_IntegralZone(0,Constants.CollectorConstants.shootingIzone);
    intakeLower.configAllowableClosedloopError(0,Constants.CollectorConstants.shootingAllowableError );
    intakeLower.configClosedLoopPeakOutput(0,Constants.CollectorConstants.shootingPeakOutput);
    
    //UpperIntkeConfigs
    intakeUpper.config_kP(0, Constants.CollectorConstants.shootingKp);
    intakeUpper.config_kI(0, Constants.CollectorConstants.shootingKi);
    intakeUpper.config_kD(0, Constants.CollectorConstants.shootingKd);
    intakeUpper.config_kF(0,Constants.CollectorConstants.shootingKf);
    intakeUpper.configMaxIntegralAccumulator(0, Constants.CollectorConstants.shootingMaxIntegralAccum);
    intakeUpper.configClosedLoopPeriod(0,Constants.CollectorConstants.shootingLoopPeriodMs);
    intakeUpper.config_IntegralZone(0,Constants.CollectorConstants.shootingIzone);
    intakeUpper.configAllowableClosedloopError(0,Constants.CollectorConstants.shootingAllowableError );
    intakeUpper.configClosedLoopPeakOutput(0,Constants.CollectorConstants.shootingPeakOutput);
    
    //DeliveryConfings
    intakeLower.setInverted(Constants.CollectorConstants.IntakeLowerInvert);
    delivery.setInverted(Constants.CollectorConstants.DeliveryInvert);

    // SmartDashboard.putNumber("ShooterVelocity", 0);
    // SmartDashboard.putNumber("Shooter Position", getArmPosition());
    // SmartDashboard.putNumber("shooter currentVelocity", getIntakeSpeeds());

    SmartDashboard.putNumber("Kp", 0);
    SmartDashboard.putNumber("Ki", 0);
    SmartDashboard.putNumber("Kf", 0);
    SmartDashboard.putNumber("Kd", 0);
    
  }
  public void shoot(double targetShooting) {
    if(targetShooting != 0 ){
      shooterVelocity(targetShooting, -targetShooting);
      if(getIntakeSpeeds() > Math.abs(targetShooting- 300) && getIntakeSpeeds() < Math.abs(targetShooting + 300)) { 
        delivery(-0.3);
      }}
  }

  public double getShooterVelocityFromDashBoard() {
    return SmartDashboard.getNumber("ShooterVelocity", 0);
  }

  public double getShooterKpFromShuffleBoard() {
    return SmartDashboard.getNumber("Kp", 0);
  }
  public double getShooterKiFromShuffleBoard() {
    return SmartDashboard.getNumber("Ki", 0);
  }
  public double getShooterKfFromShuffleBoard() {
    return SmartDashboard.getNumber("Kf", 0);
  }
  public double getShooterKdFromShuffleBoard() {
    return SmartDashboard.getNumber("Kd", 0);
  }
  public double getArmPositionFromDashboard() {
    return SmartDashboard.getNumber("arm", 0);
  }


  public void allShoot(double velocity, double armPosition) {
    shooterVelocity(velocity, -velocity);
    setArmPosition(armPosition);
    if((getIntakeSpeeds() > Math.abs(velocity- 300) && getIntakeSpeeds() < Math.abs(velocity + 300)) && (getArmPosition() > armPosition - 600 && getArmPosition() < armPosition + 600)) {
      delivery(-0.3);
    }
  }
  //u dumb fuck
  public void shooter(double velocity,double velocity_2){
    intakeLower.set(ControlMode.PercentOutput,velocity);
    intakeUpper.set(ControlMode.PercentOutput,velocity_2);


  }
  public Double getVelocityRight(){
      return intakeUpper.getSelectedSensorVelocity();
  }

  public void shootForAmp() {
    shooter(-0.4, -0.2);
    delivery(-0.2);
    setArmPosition(1000);
  }

  public double getIntakeSpeeds() {
    return Math.abs(intakeLower.getSelectedSensorVelocity());
  }

  public double getUnderIntakeSpeed() {
    return Math.abs(intakeLower.getSelectedSensorVelocity());
  }

  public void shooterVelocity(double velocity,double velocity_2){
    intakeLower.set(ControlMode.Velocity,-velocity);
    intakeUpper.set(ControlMode.Velocity, -velocity_2);

  }
  public void moveArm(double power){
    arm.set(ControlMode.PercentOutput,power);
  }
  public void delivery(double power){
    delivery.set(ControlMode.PercentOutput,power);
  }

  public boolean reachedVelocity(){
     return Math.abs(intakeLower.getSelectedSensorVelocity() - intakeLower.getClosedLoopTarget()) < 700  &&
          Math.abs(intakeUpper.getSelectedSensorVelocity() - intakeUpper.getClosedLoopTarget()) < 700;
  }

  public boolean isNoteInCollector(){  
    //blue 9244 red 8064 green 15575

    return colorSensor.getRed() >  2000 ;
  }
  public boolean isArmAtPosition(double error, double target){
    return target>getArmPosition() - error && getArmPosition() <target + error;
  }

  public void setArmPosition(double position){
    arm.set(ControlMode.Position, position);
  }

  public double getArmPosition(){
    return arm.getSelectedSensorPosition();
  }

  public double getShootingFunction(double distance){
      // return -3732.15*Math.pow(distance, 2) + 28695.94*distance -22123.64;
      return -3732.15*Math.pow(distance, 2) + 28695.94*distance -22000.64;
      // return -4784.51*Math.pow(distance, 2) + 36028.17*distance -31152.86;


  }

  // public void setAllSettingsForShooter(double Kp , double Ki , double Kd ,  double Kf){
  //   intakeLower.config_kP(0, Kp);
  //   intakeLower.config_kI(0, Ki);
  //   intakeLower.config_kD(0, Kd);
  //   intakeLower.config_kF(0,Kf);
  //   intakeLower.configMaxIntegralAccumulator(0, Constants.CollectorConstants.shootingMaxIntegralAccum);
  //   intakeLower.configClosedLoopPeriod(0,Constants.CollectorConstants.shootingLoopPeriodMs);
  //   intakeLower.config_IntegralZone(0,Constants.CollectorConstants.shootingIzone);
  //   intakeLower.configAllowableClosedloopError(0,Constants.CollectorConstants.shootingAllowableError );
  //   intakeLower.configClosedLoopPeakOutput(0,Constants.CollectorConstants.shootingPeakOutput);
    
  // }


  @Override
  public void periodic() {
    // System.out.println("blue " + colorSensor.getBlue() + " red " + colorSensor.getRed() + " green " + colorSensor.getGreen());
    // System.out.println(isNoteInCollector());
    // System.out.println(colorSensor.getColor().toString());
    // System.out.println("hjjj  " + colorSensor.isConnected());
    // SmartDashboard.putBoolean("is not inside", isNoteInCollector());    
    // SmartDashboard.putString("shooter ", "blue " + colorSensor.getBlue() + " red " + colorSensor.getRed() + " green " + colorSensor.getGreen());
    SmartDashboard.getNumber("ShooterVelocity", 0);
    SmartDashboard.putNumber("Shooter Position", getArmPosition());
    SmartDashboard.putNumber("shooter currentVelocity", getIntakeSpeeds());

    
    // System.out.println(getShooterKpFromShuffleBoard());
  }

  public boolean isArmAtPosition(){
    return Math.abs(arm.getSelectedSensorPosition() - arm.getClosedLoopTarget()) < Constants.CollectorConstants.ArmAllowableError;
  }


}


 




