package frc.robot.subsystems.Swerve;

public enum ModuleType{
    FRONT_LEFT,
    FRONT_RIGHT,
    BACK_LEFT,
    BACK_RIGHT,
    NONE;

    public final byte id;

    ModuleType(){
        this.id = id();
    }

    private byte id() {
        if (this == NONE)
            return -1;
        // This is a trick that relies on the order the enums are defined.
        return (byte) this.ordinal();
    }
}