package frc.robot.subsystems.Swerve;

import frc.robot.Constants;

/**
 * Container class for module constants, defined using constants from {@link DriveConstants}
 * .
 *
 * @see DriveConstants
 */
public enum ModuleConstants {

    FRONT_LEFT(
            Constants.Swerve.Id.kDriveFrontLeft,
            Constants.Swerve.Id.kSteerFrontLeft,
            Constants.Swerve.Id.kEncoderFrontLeft,
            Constants.Swerve.Id.kSteerOffsetFrontLeft,
            ModuleType.FRONT_LEFT
    ),
    FRONT_RIGHT(
            Constants.Swerve.Id.kDriveFrontRight,
            Constants.Swerve.Id.kSteerFrontRight,
            Constants.Swerve.Id.kEncoderFrontRight,
            Constants.Swerve.Id.kSteerOffsetFrontRight,
            ModuleType.FRONT_RIGHT
    ),
    BACK_LEFT(
            Constants.Swerve.Id.kDriveBackLeft,
            Constants.Swerve.Id.kSteerBackLeft,
            Constants.Swerve.Id.kEncoderBackLeft,
            Constants.Swerve.Id.kSteerOffsetBackLeft,
            ModuleType.BACK_LEFT
    ),
    BACK_RIGHT(
            Constants.Swerve.Id.kDriveBackRight,
            Constants.Swerve.Id.kSteerBackRight,
            Constants.Swerve.Id.kEncoderBackRight,
            Constants.Swerve.Id.kSteerOffsetBackRight,
            ModuleType.BACK_RIGHT
    ),

    NONE(0, 0, 0, 0.0, ModuleType.NONE);

    private final int drivePort;
    private final int steerPort;
    private final int encoderPort;
    private final double steerOffset;
    private final ModuleType type;

    ModuleConstants(
            int drivePort,
            int steerPort,
            int encoderPort,
            double steerOffset,
            ModuleType type
                   ) {
        this.drivePort = drivePort;
        this.steerPort = steerPort;
        this.encoderPort = encoderPort;
        this.steerOffset = steerOffset;
        this.type = type;
    }

    public int getDrivePort() {
        return drivePort;
    }

    public int getSteerPort() {
        return steerPort;
    }

    public int getEncoderPort() {
        return encoderPort;
    }

    public double getSteerOffset() {
        return steerOffset;
    }

    public ModuleType getType() {
        return type;
    }

}