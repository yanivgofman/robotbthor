package frc.robot;

import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.NestingKind;
import javax.print.DocFlavor.STRING;
import javax.xml.transform.stax.StAXResult;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.pathplanner.lib.util.PIDConstants;

import edu.wpi.first.apriltag.AprilTag;
import edu.wpi.first.math.MatBuilder;
import edu.wpi.first.math.Matrix;
import edu.wpi.first.math.Nat;
import edu.wpi.first.math.Pair;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.numbers.N1;
import edu.wpi.first.math.numbers.N3;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.networktables.PubSub;
import edu.wpi.first.wpilibj.PowerDistribution;
import frc.robot.Constants.FieldConstants;
import frc.robot.subsystems.Swerve.COTSFalconSwerveConstants;


public final class Constants {

  public static double LoopTime = 0.02;

  public static double RobotVoltage = 12.0;

  public static boolean Telemetry = true;

  public static boolean logging = true;
  
  public static class Swerve {

    // public static double SpeakerRedSideY = 2.5;
    // public static double SpeakerBlueSideY = 5;

    public static double distance = 0.0;
    //width with bumpers
    public static double kRobotWidthWithBumpers = Units.inchesToMeters(Units.metersToInches(0.65));

    //width witdout bumpers
    public static double kTrackWidth = 0.65;

    //radius of the robot
    public static double kRobotRadius = Math.sqrt( 2 * Math.pow(kTrackWidth, 2));

    //steer wheels radius
    public static double kWheelRadius = Units.inchesToMeters(2);

    //Swerve Module MK4_L2 gear ratios caculated
    public static double kDriveGearRatio = (50.0 / 14.0) * (17.0 / 27.0) * (45.0 / 15.0);
    public static double kSteerGearRatio = 150.0 / 7.0;

    //FeedForward variables
    /* Drive Motor Characterization Values
     * Divide SYSID values by 12 to convert from volts to percent output for CTRE */
    public static final double DRIVE_KS = 0.32 / 12.0; // 0.65559
    public static final double DRIVE_KV = 1.51 / 12.0; // 1.93074
    public static final double DRIVE_KA = 0.27 / 12.0; // 0.00214

    //robot starting angle
    public static Rotation2d kStartingHeading = new Rotation2d();

    
    //max speed calcuated 
    public static double kMaxSpeed = (FalconConstants.MAX_RPM / 80.0) * kWheelRadius * 2 * Math.PI / kDriveGearRatio;

    //max angular speed
    public static double kMaxAngularSpeed = kMaxSpeed / ((kTrackWidth / 2) * Math.sqrt(2));

    //max angular acceleration
    public static double kMaxAngularAccel = 4 * 2 * Math.PI; // 8 rotations per second per second


    //swerve Falcon Constants
    public static final COTSFalconSwerveConstants kModuleConstants = COTSFalconSwerveConstants.SDSMK4i(COTSFalconSwerveConstants.DriveGearRatios.SDSMK4i_L2);

    /* Swerve Current Limiting */
    public static final int kAngleContinuousCurrentLimit = 25;
    public static final int kAnglePeakCurrentLimit = 40;
    public static final double kAnglePeakCurrentDuration = 0.1;
    public static final boolean kAngleEnableCurrentLimit = true;
    public static final int kDriveContinuousCurrentLimit = 35;
    public static final int kDrivePeakCurrentLimit = 60;
    public static final double kDrivePeakCurrentDuration = 0.1;
    public static final boolean kDriveEnableCurrentLimit = true;


    /* Motor inversions */
    public static final boolean kDriveMotorInvert = true;//kModuleConstants.driveMotorInvert;
    public static final boolean kAngleMotorInvert = kModuleConstants.angleMotorInvert;

    /* Neutral Modes */
    public static final NeutralMode kDriveNeutralMode = NeutralMode.Brake;
    public static final NeutralMode kAngleNeutralMode = NeutralMode.Coast;

    /* Drive Motor PID Values */
    public static final double kDriveP = 0.05;
    public static final double kDriveI = 0.0;
    public static final double kDriveD = 0.0;
    public static final double kDriveF = 0.0;

    /* Ramp values for drive motors in open and closed loop driving. */
    // Open loop prevents throttle from changing too quickly.
    // It will limit it to time given (in seconds) to go from zero to full throttle.
    // A small open loop ramp (0.25) helps with tread wear, tipping, etc
    public static final double kOpenLoopRamp = 0.25;
    public static final double kClosedLoopRamp = 0.0;

    public static final double kWheelCircumference = kModuleConstants.wheelCircumference;

    /* Motor gear ratios */
    public static final double kAngleGearRatio = kModuleConstants.angleGearRatio;

    public static final boolean kInvertGyro = false; // Make sure gyro is CCW+ CW- // FIXME: Swerve

    public static final double kSlowDriveFactor = 0.2;
    public static final double kSlowRotFactor = 0.1;

    public static final SwerveDriveKinematics Kinematics = new SwerveDriveKinematics(
            new Translation2d(kTrackWidth / 2, kTrackWidth / 2),
            new Translation2d(kTrackWidth / 2, -kTrackWidth / 2),
            new Translation2d(-kTrackWidth / 2, kTrackWidth / 2),
            new Translation2d(-kTrackWidth / 2, -kTrackWidth / 2)
    );

    //translational PID
    public static double kTranslationalP = 4;
    public static double kTranslationalD = 0;//0.001

    //The PIDs for PathPlanner Command
    public static double kPathplannerHeadingP = 0;
    public static double kPathplannerHeadingD = 0;

    public static double kPathplannerTranslationalP = 6;
    public static double kPathplannerTranslationalD = 0;

    // heading PID
    //4.6
    public static double kHeadingP = 2.7;
    public static double kHeadingD = 0.00045;

    //pid for traking the ring
    public static double kRingP = 0.12;

    //swerve components id
    public static class Id{
      public static final int kPigeonId = 40;

      public static final int kDriveFrontLeft = 43;
      public static final int kSteerFrontLeft = 42;
      public static final int kEncoderFrontLeft = 41;
      public static final double kSteerOffsetFrontLeft = Math.toRadians(23.203);//-3.060285486280918+Math.PI;
      // public static final double kSteerOffsetFrontLeft = Math.toRadians(201);//-3.060285486280918+Math.PI;

  
      public static final int kDriveFrontRight = 33;
      public static final int kSteerFrontRight = 32;
      public static final int kEncoderFrontRight = 31;
      public static final double kSteerOffsetFrontRight = Math.toRadians(246.182);//159.285;
      // public static final double kSteerOffsetFrontRight = Math.toRadians(155);//159.285;

  
      public static final int kDriveBackLeft = 13;
      public static final int kSteerBackLeft = 12;
      public static final int kEncoderBackLeft = 11;
      public static final double kSteerOffsetBackLeft = Math.toRadians(174.990);//0.650406539440155+Math.PI;
      // public static final double kSteerOffsetBackLeft = Math.toRadians(124);//159.285;

  
      public static final int kDriveBackRight = 23;
      public static final int kSteerBackRight = 22;
      public static final int kEncoderBackRight = 21;
      public static final double kSteerOffsetBackRight = Math.toRadians(211.729);//2.771897681057453;
      // public static final double kSteerOffsetBackRight = Math.toRadians(156);//2.771897681057453;

  
    }
   

  }

  

public class VisionConstants {

       public static final double kVisionPoseStdDevFactor = 1.0;

      /**
       * If April tag vision is enabled on the robot
       */
      public static final boolean ENABLED = true;

      // If odometry should be updated using vision during auto
      public static final boolean ENABLED_AUTO = false;

      // If odometry should be updated using vision while running the GoToPose and GoToPosePID commands in teleop
      public static final boolean ENABLED_GO_TO_POSE = true;

      // If vision should use manual calculations
      public static final boolean USE_MANUAL_CALCULATIONS = true;

      // The number to multiply the distance to the April tag by
      // Only affects manual calculations
      // To find this, set it to 1 and measure the actual distance and the calculated distance
      public static final double DISTANCE_SCALE = 0.85;

      /*
      * The standard deviations to use for the vision
      */
      public static final Matrix<N3, N1> VISION_STD_DEVS = MatBuilder.fill(Nat.N3(), Nat.N1(),
      0.00443, // x in meters (default=0.9)
      0.00630, // y in meters (default=0.9)
      1000  // heading in radians. The gyroscope is very accurate, so as long as it is reset correctly it is unnecessary to correct it with vision
    );
      // The highest ambiguity to use. Ambiguities higher than this will be ignored.
      // Only affects calculations using PhotonVision, not manual calculations
      public static final double HIGHEST_AMBIGUITY = 0.02;

      // Speaker poses
      public static final Pose3d BLUE_SPEAKER_POSE = new Pose3d(
        FieldConstants.APRIL_TAGS.get(6).pose.getX() + Units.inchesToMeters(9),
        FieldConstants.APRIL_TAGS.get(6).pose.getY(),
        Units.inchesToMeters(80.5),
        new Rotation3d(0, 0, 0)
      );
      public static final Pose3d RED_SPEAKER_POSE = new Pose3d(
        FieldConstants.APRIL_TAGS.get(3).pose.getX() - Units.inchesToMeters(9),
        BLUE_SPEAKER_POSE.getY(),
        BLUE_SPEAKER_POSE.getZ(),
        BLUE_SPEAKER_POSE.getRotation().rotateBy(new Rotation3d(0, 0, Math.PI))
      );

      // The amp poses to align to
      public static final Pose2d BLUE_AMP_POSE = new Pose2d(
        FieldConstants.APRIL_TAGS.get(5).pose.getX(),
        FieldConstants.APRIL_TAGS.get(5).pose.getY() - Swerve.kRobotWidthWithBumpers/2,
        new Rotation2d(Math.PI/2)
      );
      public static final Pose2d RED_AMP_POSE = new Pose2d(
        FieldConstants.APRIL_TAGS.get(4).pose.getX(),
        BLUE_AMP_POSE.getY(),
        BLUE_AMP_POSE.getRotation()
      );

      // The podium poses to align to
      public static final Pose2d BLUE_PODIUM_POSE = new Pose2d(
        FieldConstants.APRIL_TAGS.get(13).pose.getX() - Units.inchesToMeters(82.75) - Swerve.kRobotWidthWithBumpers/2,
        FieldConstants.APRIL_TAGS.get(13).pose.getY(),
        new Rotation2d(Math.PI)
      );
      public static final Pose2d RED_PODIUM_POSE = new Pose2d(
        FieldConstants.APRIL_TAGS.get(12).pose.getX() + Units.inchesToMeters(82.75) + Swerve.kRobotWidthWithBumpers/2,
        BLUE_PODIUM_POSE.getY(),
        new Rotation2d(Math.PI).minus(BLUE_PODIUM_POSE.getRotation())
      );
      // TODO: check/tune vision weight
      // How much to trust vision measurements normally

      // TODO: check/tune vision weight
      // How much to trust vision measurements normally
      public static final Matrix<N3, N1> kBaseVisionPoseStdDevs = new MatBuilder<>(Nat.N3(), Nat.N1()).fill(
        0, // x in meters (default=0.9)
        0, // y i meters (default=0.9)
        1000  // heading in radians. The gyroscope is very accurate, so as long as it is reset correctly it is unnecessary to correct it with vision
      );

      public static final Matrix<N3, N1> kBaseVPoseStdDevs = new MatBuilder<>(Nat.N3(), Nat.N1()).fill(
        0.9, // x in meters (default=0.9)
        0.9, // y in meters (default=0.9)
        1000  // heading in radians. The gyroscope is very accurate, so as long as it is reset correctly it is unnecessary to correct it with vision
      );
      // How much to trust vision after passing over the charge station
      // Should be lower to correct pose after charge station
      public static final Matrix<N3, N1> kChargeStationVisionPoseStdDevs = new MatBuilder<>(Nat.N3(), Nat.N1()).fill(
        0.01, // x in meters
        0.01, // y in meters
        1000   // heading in radians. The gyroscope is very accurate, so as long as it is reset correctly it is unnecessary to correct it with vision
      );

      // If vision is enabled
      public static final boolean kEnabled = true;

      // The angle to use charge station vision at in degrees. 
      // If the pitch or the roll of the robot is above this amount, it will trust vision more for a bit.
      public static final double kChargeStationAngle = 2.5;

      //TODO: Change these to whatever the actual distances are
      /**  How far from the grid the robot should be to score for alignment, in meters */
      public  double kGridDistanceAlignment = 0 + Swerve.kRobotWidthWithBumpers / 2; // meters
      
      /** How far from the shelf the robot should be to intake for alignment, in meters */
      public  double kShelfDistanceAlignment = 0.5 + Swerve.kRobotWidthWithBumpers / 2;

      public static final Transform3d LimeLightAuto = new Transform3d(
                //0.3875
                new Translation3d(Units.inchesToMeters(-15),Units.inchesToMeters(0),0),
                new Rotation3d(Units.degreesToRadians(120), 0.0,Math.PI));

        public static final Transform3d LimeLightTeleop = new Transform3d(
                //0.3875
                new Translation3d(Units.inchesToMeters(17.5),Units.inchesToMeters(-20),0),
                new Rotation3d(Units.degreesToRadians(120), 0.0,0.0));

      public static final Transform3d CAMERA_TO_ROBOT = new Transform3d(
                //0.3875
                new Translation3d(0,Units.inchesToMeters(0),Units.inchesToMeters(20)),
                new Rotation3d(Units.degreesToRadians(120),0.0,0.0));

  }

    
    public static class AutoConstants{    
    public static final double wheelRotationPerSecond = 2; // m/s
    public static final double MAX_AUTO_ACCEL = 2; // m/s^%2

    public static PIDConstants translationController = new PIDConstants(4, 0, 0);
    public static PIDConstants rotationController = new PIDConstants(6,0, 0);


  }

  public class FalconConstants{
    
    public static final int FIRMWARE_VERSION = 5633; // version 22.1.1.0
    public static final boolean BREAK_ON_WRONG_FIRMWARE = false; // TODO: fix issue that make the robot break

    public static final double RESOLUTION = 2048;
    public static final double MAX_RPM = 6380.0; // Rotations per minute

    // These are the default values

    // Stator
    public static final boolean STATOR_LIMIT_ENABLE = false; // enabled?
    public static final double STATOR_CURRENT_LIMIT = 100; // Limit(amp
    public static final double STATOR_TRIGGER_THRESHOLD = 100; // Trigger Threshold(amp)
    public static final double STATOR_TRIGGER_DURATION = 0; // Trigger Threshold Time(s)

    // Supply
    public static final boolean SUPPLY_LIMIT_ENABLE = false; // enabled?
    public static final double SUPPLY_CURRENT_LIMIT = 40; // Limit(amp), current to hold after trigger hit
    public static final double SUPPLY_TRIGGER_THRESHOLD = 55; // (amp), amps to activate trigger
    public static final double SUPPLY_TRIGGER_DURATION = 3; // (s), how long after trigger before reducing

  }

  
  public static class FieldConstants {
      public static final double kFieldLength = Units.inchesToMeters(54*12 + 3.25); // meters
      public static final double kFieldWidth = Units.inchesToMeters(26*12 + 11.25); // meters
    
      // Array to use if it can't find the April tag field layout
      public static final ArrayList<AprilTag> APRIL_TAGS = new ArrayList<AprilTag>(List.of(
        new AprilTag(1, new Pose3d(Units.inchesToMeters(593.68), Units.inchesToMeters(9.68), Units.inchesToMeters(53.38), new Rotation3d(0, 0, 2*Math.PI/3))),
        new AprilTag(2, new Pose3d(Units.inchesToMeters(637.21), Units.inchesToMeters(34.79), Units.inchesToMeters(53.38), new Rotation3d(0, 0, 2*Math.PI/3))),
        new AprilTag(3, new Pose3d(Units.inchesToMeters(652.73), Units.inchesToMeters(196.17), Units.inchesToMeters(57.13), new Rotation3d(0, 0, Math.PI))),
        new AprilTag(4, new Pose3d(Units.inchesToMeters(652.73), Units.inchesToMeters(218.42), Units.inchesToMeters(57.13), new Rotation3d(0, 0, Math.PI))),
        new AprilTag(5, new Pose3d(Units.inchesToMeters(578.77), Units.inchesToMeters(323.00), Units.inchesToMeters(53.38), new Rotation3d(0, 0, -Math.PI/2))),
        new AprilTag(6, new Pose3d(Units.inchesToMeters(72.5), Units.inchesToMeters(323.00), Units.inchesToMeters(53.38), new Rotation3d(0, 0, -Math.PI/2))),
        new AprilTag(7, new Pose3d(Units.inchesToMeters(-1.50), Units.inchesToMeters(218.42), Units.inchesToMeters(57.13), new Rotation3d(0, 0, 0))),
        new AprilTag(8, new Pose3d(Units.inchesToMeters(-1.50), Units.inchesToMeters(196.17), Units.inchesToMeters(57.13), new Rotation3d(0, 0, 0))),
        new AprilTag(9, new Pose3d(Units.inchesToMeters(14.02), Units.inchesToMeters(34.79), Units.inchesToMeters(53.38), new Rotation3d(0, 0, Math.PI/3))),
        new AprilTag(10, new Pose3d(Units.inchesToMeters(57.54), Units.inchesToMeters(9.68), Units.inchesToMeters(53.38), new Rotation3d(0, 0, Math.PI/3))),
        new AprilTag(11, new Pose3d(Units.inchesToMeters(468.69), Units.inchesToMeters(146.19), Units.inchesToMeters(52.00), new Rotation3d(0, 0, -Math.PI/3))),
        new AprilTag(12, new Pose3d(Units.inchesToMeters(468.69), Units.inchesToMeters(177.10), Units.inchesToMeters(52.00), new Rotation3d(0, 0, Math.PI/3))),
        new AprilTag(13, new Pose3d(Units.inchesToMeters(441.74), Units.inchesToMeters(161.62), Units.inchesToMeters(52.00), new Rotation3d(0, 0, Math.PI))),
        new AprilTag(14, new Pose3d(Units.inchesToMeters(209.48), Units.inchesToMeters(161.62), Units.inchesToMeters(52.00), new Rotation3d(0, 0, 0))),
        new AprilTag(15, new Pose3d(Units.inchesToMeters(182.73), Units.inchesToMeters(177.10), Units.inchesToMeters(52.00), new Rotation3d(0, 0, 2*Math.PI/3))),
        new AprilTag(16, new Pose3d(Units.inchesToMeters(182.73), Units.inchesToMeters(146.19), Units.inchesToMeters(52.00), new Rotation3d(0, 0, -2*Math.PI/3)))
      ));
    }  

  public class OIConstants {
  
      public static final int DRIVER_JOY = 0;
  
      public static final int OPERATOR_JOY = 1;
      public static final int TEST_JOY = 2;
      public static final int MANUAL_JOY = 3;
      public static final double DEADBAND = 0.005;
  
      //TODO: change sensitivity to 1?
  
      public static final double TRANSLATIONAL_SENSITIVITY = 1;
      public static final double TRANSLATIONAL_EXPO = 2;
      public static final double TRANSLATIONAL_DEADBAND = 0.05;
      public static final double TRANSLATIONAL_SLEWRATE = 20;
      public static final boolean FIELD_RELATIVE = true;
      public static final double ROTATION_SENSITIVITY = 1;
  
      public static final double ROTATION_EXPO = 4;
      public static final double ROTATION_DEADBAND = 0.01;
      public static final double ROTATION_SLEWRATE = 10;
      public static final double HEADING_SENSITIVITY = 4;
  
      public static final double HEADING_EXPO = 2;
      public static final double HEADING_DEADBAND = 0.05;
      public static final boolean CONSTANT_HEADING_MAGNITUDE = false;
      public static final boolean INVERT = false;

  }

  public static class CollectorConstants{

    public static NeutralMode ShootingWheelsNeutralMode = NeutralMode.Coast;
    public static int LowerShootingWheelsId = 53;
    public static int UpperShootingWheelsId = 6;

    public static int DeliveryMotorId = 5;
    public static NeutralMode DelivertNeutralMode = NeutralMode.Coast;

    public static int ArmMotorID = 48;
    public static NeutralMode ArmNeutralMode = NeutralMode.Brake;

    public static double ArmKp = 0.06;    
    public static double ArmKi = 0.0015;
    public static double ArmKd = 0;
    public static double ArmKf = 0;

    public static double ArmAllowableError = 600;

    public static boolean DeliveryInvert = true;
    public static boolean IntakeLowerInvert = true;

    public static double CollectingPosition = 189772;
    public static double ClosingPosition = 10000;

    public static double shootingKp = 0.11;
    public static double shootingKi = 0.0002;
    public static double shootingKd = 0.015;
    public static double shootingKf = 0.05;
    public static double shootingIzone = 0;
    public static int shootingLoopPeriodMs = 20;
    public static double shootingAllowableError = 0;
    public static double shootingPeakOutput = 1;
    public static double shootingMaxIntegralAccum = 0;


  }
  

  public static class ElevatorConstants{
    public static int ElevatorId = 3;
    public static NeutralMode ElevatorNeutralMode = NeutralMode.Brake;
    public static double ElevatorKp = 0.1;
    public static double ElevatorKi = 0;
    public static double ElevatorKd = 0;
    public static double ElevatorKf = 0;
    public static double ElevatorAllowableError = 0;
    public static double ElevatorPeakOutput = 1;
    

    public static final int ClimbID = 4;
    public static NeutralMode ClimbNeutralMode = NeutralMode.Brake;
    public static double ClimbKp = 0.04;
    public static double ClimbKi = 0;
    public static double ClimbKd = 0;
    public static double ClimbKf = 0;
    public static double ClimbAllowableError = 0;
    public static double ClimbPeakOutput = 0.3;



    //SlideConfigs
    public static final int SlideID = 58;
    public static NeutralMode SlideNeutralMode = NeutralMode.Brake;
    public static double SlideKp = 0.05;
    public static double SlideKi = 0;
    public static double SlideKd = 0;
    public static double SlideKf = 0;
    public static double SlideAllowableError = 0;
    public static double SlidePeakOutput = 0.3;

    //IntakeConfigs
    public static final int IntakeID = 51;
    public static NeutralMode IntakeNeutralMode = NeutralMode.Coast;

  }
}