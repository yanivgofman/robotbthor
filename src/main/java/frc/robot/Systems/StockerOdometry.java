// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.Systems;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.Swerve.SwerveSubsystem;

public class StockerOdometry extends Command {
  SwerveSubsystem swerve;
  Joystick joystick;
  double targetAngle = 0;
  public StockerOdometry(SwerveSubsystem swerve, Joystick joystick) {
    this.swerve = swerve;
    this.joystick = joystick;
    addRequirements(swerve);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    if(swerve.getStockerAngle() > 0)
      targetAngle = -(90 + swerve.getStockerAngle());
    else if(swerve.getStockerAngle() < 0)
      targetAngle = 90 + Math.abs(-swerve.getStockerAngle());


    targetAngle += 180;
    System.out.println("target angle: " + targetAngle);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double forwardTranslation = joystick.getRawAxis(1);
    double sideTranslation = joystick.getRawAxis(0);


    swerve.driveHeadingStocker(
      0,
      0, 
      targetAngle, 
      true);
    System.out.println("target angle: " + targetAngle);

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    System.out.println("STOCKER ENDED!!!!!!!!!!!!!!!!!!!!!!");
    swerve.stop();
  } 

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    System.out.println("target: " + targetAngle + " gyro: " + swerve.getYaw().getDegrees());

    if(Math.abs(joystick.getRawAxis(2)) > 0.2 || Math.abs(Math.abs(targetAngle) - Math.abs(swerve.getYaw().getDegrees())) <5) {
      return true;
    }
    return false;
  }
}
